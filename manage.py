import os
import pytest
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
import config
from models.tolotolo_model import db

#setting up with sqlite
app = Flask(__name__)
app.config.from_object(config.DevelopmentConfig)

# add flask plugins here
db.app = app
db.init_app(app)
migrate = Migrate(app, db, directory=config.DevelopmentConfig.MIGRATION_DIR)

#manager manages all
manager = Manager(app)
manager.add_command('db', MigrateCommand)

#commands

@manager.command
def run_tests(db=False, api=False, s=False):
    """
        Run Test, default to all
        
        options:
        > --db: run test on database
        > --api: run test on api
        > --s: don't ignore print statement
    """
    if s:
        s = ['-s']
    else:
        s =[]
    
    def test_db():
        print('>>> Running tests on models')
        pytest.main(s + ['tests/test_db.py'])
    def test_ex():
        print('>>> Running tests on exchange logic')
        pytest.main(s + ['tests/test_ex.py'])
    def test_api():
        print('>>> Running tests on api and routes')
        pytest.main(s + ['tests/test_api.py'])
    if db:
        test_db()
    if api:
        test_api()
    if not (api or db):
        pytest.main(s + ['tests'])