# Flask python-telegram-bot 

### Requirements

- python 3+
- virtualenv
- ngrok (for dev and testing)

### steps

- clone this repository

   `git clone https://eltneg@bitbucket.org/protonlabs/bot.git`

- create a .env file in the base directory. Add a debugging key (optional), database uri (mysql or postgress), telegram bot token (from [botfather](t.me/botfather) ) and an https url (from [ngrok](https://ngrok.com/)).

  > KEY=SECRET_KEY
  >
  > DATABASE_URI=postgres://db_user:db_host.com:5432/db_name
  >
  > BOT_TOKEN=bot_token
  >
  > PUBLIC_URL=https://ecf64e44.ngrok.io

- create a new virtual environment

  `mkvirtualenv new_env`

- install requirements

  `pip install -r requirements.txt`

- set up database

  `python app.py db init`

  `python app.py db migrate`

  `python app.py db upgrade`

- run the bot in development mode

  `python app.py runserver`

### Todo

- add tests
- automate set up for windows and linux
- add production command
- add demo bots