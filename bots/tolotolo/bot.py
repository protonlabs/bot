# -*- coding: utf-8 -*-
import os, sys
from threading import Thread
import logging
from random import randint
from time import sleep

from telegram.ext import InlineQueryHandler
from telegram import (InlineQueryResultArticle, InputTextMessageContent)
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import RegexHandler
from telegram.ext import Filters
from telegram import ParseMode
from telegram.ext import JobQueue

from telegram import (ReplyKeyboardRemove, ReplyKeyboardMarkup)
from telegram import (InlineKeyboardButton, InlineKeyboardMarkup)
from telegram.ext import (ConversationHandler) # , RegexHandler)
from telegram.ext import CallbackQueryHandler

from .settings import Setup, bot_instance

CHANNEL_MONITOR = '@channelname'
BOT = bot_instance()
update_queue, dispatcher = Setup(BOT)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
job_queue = JobQueue(BOT)
job_queue.start()
dispatcher.job_queue = job_queue


#=========BOT commands==============================

def stop_and_restart():
        """Gracefully stop the Updater and replace the current process with a new one"""
        job_queue.stop()
        os.execl(sys.executable, sys.executable, *sys.argv)

def restart(BOT, update):
    update.message.reply_text('BOT is restarting...')
    Thread(target=stop_and_restart).start()

dispatcher.add_handler(CommandHandler('r', restart, filters=Filters.user(username='@eltNEG')))

#===========================COMMANDS======================================================================

def start(BOT, update):
    BOT.send_message(chat_id=update.message.chat_id, text="Hello")
     #disable_web_page_preview=True, parse_mode=ParseMode.HTML)    
dispatcher.add_handler(CommandHandler('start', start))

def webhook(update):
    update_queue.put(update)