from .base_model import BaseModel, db

class User(BaseModel):
    __Tablename__ = 'User'

    _id = db.Column(db.Integer, primary_key=True)
    _uid = db.Column(db.String(30), unique=True) #hash or something unique
    _tid = db.Column(db.Integer, unique=True)

    #_wallet = db.relationship('Wallet', backref="user", cascade="all, delete", lazy="dynamic")
    
    def __init__(self, user_id, telegram_id):
        self._uid = user_id
        self._tid = telegram_id
        #new_wallet = Wallet(wallet_name, wallet_for, wallet_address)
        #self._wallet.append(new_wallet)
        #db.session.add(new_wallet)
