from flask import request, jsonify
import telegram
from manage import manager, app

#import bots
from bots.tolotolo.bot import BOT, webhook

# ============setup static files=======================
#app.config['STATIC_FOLDER'] = './webClient/build'

# ===============================================

@app.route('/', methods=['POST', 'GET'])
def home():
    if request.method == 'POST':
        return 'Hello World-- post req', 200
    else:
        return 'Hello World-- GET req', 200

@app.route('/bot/tolotolo', methods=['POST', 'GET'])
def botIndex():
    if request.method == 'POST':
        update = telegram.Update.de_json(request.get_json(force=True), BOT)
        webhook(update)
        return 'ok', 200


if __name__ == '__main__':
    manager.run()
